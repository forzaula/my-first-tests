import { MoviesService } from './movies.service';
import { Test, TestingModule } from '@nestjs/testing';
import { NotFoundException } from "@nestjs/common";


describe('MoviesService', () => {
  let service: MoviesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MoviesService],
    }).compile();

    service = module.get<MoviesService>(MoviesService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  })

  describe('testing function getAll',()=>{

    it('Should be array',()=>{
      const result=service.getAll();
      expect(result).toBeInstanceOf(Array)
    })
  })
  describe('testing function getOne',()=>{
    it('should be one movie',()=>{
      service.create({
        title:'test movie',
        genres:['Test genre'],
        year:2000,
      })
      const movie=service.getOne(1);
      expect(movie).toBeDefined();
      expect(movie.id).toEqual(1)

    })
    it('Error number 404',()=>{
      try {
        service.getOne(9999)
      }catch (e){
        expect(e).toBeInstanceOf(NotFoundException)
        expect(e.message).toEqual(`No movie with id:9999`)
      }
    })
  })
  describe('Test for function remove',()=>{
    it('Movie is deleted',()=>{
      service.create({
        title:'test movie',
        genres:['Test genre'],
        year:2000,
      })
      const allMovies=service.getAll()
      service.remove(1)
      const afterRemove=service.getAll()
      expect(afterRemove.length).toBeLessThan(allMovies.length)
    })
    it('should be 404 error',()=>{
      try {
        service.remove(9999)
      }catch (e){
        expect(e).toBeInstanceOf(NotFoundException)
        expect(e.message).toEqual(`No movie with id:9999`)
      }
    })

  })
  describe('test for function create',()=>{
    it('movie is created',()=>{
      const beforeCreate=service.getAll().length
      service.create({
        title:'test movie',
        genres:['Test genre'],
        year:2000,
      })
      const afterCreate=service.getAll().length
      expect(afterCreate).toBeGreaterThan(beforeCreate)
    })
  });
  describe('test for patch function',()=>{
    it('movie is updated',()=>{
      service.create({
        title:'test movie',
        genres:['Test genre'],
        year:2000,
      });
      service.patch(1,{title:"uuu"})
      const movie=service.getOne(1)
      expect(movie.title).toEqual('uuu')
    });
    it('should be 404 error',()=>{
      try {
        service.patch(9999,{title:''})
      }catch (e){
        expect(e).toBeInstanceOf(NotFoundException)
        expect(e.message).toEqual(`No movie with id:9999`)
      }
    });
  })
})